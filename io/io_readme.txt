/**
 * @file io_readme.txt
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 17 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @page OS_MODULES_IO IO - Input/Output Modules
 *
 * @section IO_INTRO Introduction to IO
 *
 * @section IO_MODULES IO Submodules
 * - @subpage OS_MODULES_IO_GPIO
 * - @subpage OS_MODULES_IO_SPI
 */
