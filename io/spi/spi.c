/**
 * @file spi.c
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 30 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license
 * This project is released under the GNU Public License (GPLv3).
 *
 * @brief Serial Peripheral Interface header implementation
 */

#include <stdbool.h>
#include <stdint.h>

#include "application.h"
#include "io/spi/spi.h"
#include "include/utils.h"

err_t SPI_disable(SPI_module_t module)
{
    return platform_spi_disable(module);
}

err_t SPI_enable(SPI_module_t module)
{
    return platform_spi_enable(module);
}

err_t SPI_init()
{
    for(uint8_t i = 0; i < ARRAY_COUNT(app_spis); i++)
    {
        SPI_disable(app_spis[i].module);
        SPI_set_byte_order(app_spis[i].module, app_spis[i].msb_first);
        SPI_set_clksrc_div(app_spis[i].module, app_spis[i].clk_src, app_spis[i].clk_div);
        SPI_set_clkpolarity(app_spis[i].module, app_spis[i].clk_polarity);
        SPI_set_clkphase(app_spis[i].module, app_spis[i].clk_phase);
        SPI_enable(app_spis[i].module);
    }

    return SUCCESS;
}

bool SPI_is_busy(SPI_module_t module)
{
    return platform_spi_is_busy(module);
}

err_t SPI_set_byte_order(SPI_module_t module, bool msb_first)
{
    return platform_spi_set_byte_order(module, msb_first);
}

err_t SPI_set_clkpolarity(SPI_module_t module, SPI_CPHA_t phase)
{
    return platform_spi_set_clkpolarity(module, phase);
}

err_t SPI_set_clkphase(SPI_module_t module, SPI_CPOL_t polarity)
{
    return platform_spi_set_clkphase(module, polarity);
}

err_t SPI_set_clksrc_div(SPI_module_t module, SPI_clk_src_t src, SPI_clk_div_t div)
{
    return platform_spi_set_clksrc_div(module, src, div);
}

uint8_t SPI_transact(SPI_module_t module, uint8_t tx)
{
    return platform_spi_transact(module, tx);
}
