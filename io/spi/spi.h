/**
 * @file spi.h
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 30 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license
 * This project is released under the GNU Public License (GPLv3).
 *
 * @brief Serial Peripheral Interface header implementation
 */

#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "include/error.h"
#include "architecture/platform.h"

typedef enum {
    SPI_CPHA_0_CAPTUREFIRST,    ///< Data is captured on the first, active edge of the clock
    SPI_CPHA_1_CHANGEFIRST      ///< Data is captured on the second, idle edge of the clock
} SPI_CPHA_t;

typedef enum {
    SPI_CPOL_0_ACTIVEHIGH,
    SPI_CPOL_1_ACTIVELOW
} SPI_CPOL_t;

#define SPI_MSB_FIRST true
#define SPI_LSB_FIRST false

typedef struct {
    SPI_module_t    module;
    SPI_clk_src_t   clk_src;
    SPI_clk_div_t   clk_div;
    SPI_CPOL_t      clk_polarity;
    SPI_CPHA_t      clk_phase;
    bool            msb_first;
} SPI_config_t;

extern err_t   platform_spi_enable(SPI_module_t module);
extern err_t   platform_spi_disable(SPI_module_t module);
extern err_t   platform_spi_set_byte_order(SPI_module_t module, bool msb_first);
extern err_t   platform_spi_set_clkpolarity(SPI_module_t module, SPI_CPHA_t phase);
extern err_t   platform_spi_set_clkphase(SPI_module_t module, SPI_CPOL_t polarity);
extern err_t   platform_spi_set_clksrc_div(SPI_module_t module, SPI_clk_src_t src, SPI_clk_div_t div);
extern uint8_t platform_spi_transact(SPI_module_t module, uint8_t tx);
extern bool    platform_spi_is_busy(SPI_module_t module);

err_t   SPI_init();
err_t   SPI_enable(SPI_module_t module);
err_t   SPI_disable(SPI_module_t module);
err_t   SPI_set_byte_order(SPI_module_t module, bool msb_first);
err_t   SPI_set_clkpolarity(SPI_module_t module, SPI_CPHA_t phase);
err_t   SPI_set_clkphase(SPI_module_t module, SPI_CPOL_t polarity);
err_t   SPI_set_clksrc_div(SPI_module_t module, SPI_clk_src_t src, SPI_clk_div_t div);
uint8_t SPI_transact(SPI_module_t module, uint8_t tx);
bool    SPI_is_busy(SPI_module_t module);

#endif /* SPI_H */
