/**
 * @file spi_readme.txt
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 17 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @page OS_MODULES_IO_SPI SPI - Serial Peripheral Interface
 *
 * @section IO_SPI_INTRO Introduction to SPI
 *
 * @section IO_SPI_APP_CONFIGS Application Configuration Values
 * The CORE module implements the following possible configuration values exposed to the
 * {@link APPLICATION_CONFIGURATION application header}.
 *
 * 1. <code>APP_USES_SPI</code> - Whether any SPI code should be included in the build or not
 */
