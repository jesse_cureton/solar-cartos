/**
 * @file can.h
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 13 November 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license
 * This project is released under the GNU Public License (GPLv3).
 *
 * @brief MCP2515 CAN driver
 */

#ifndef _CAN_H
#define _CAN_H

#include "include/error.h"
#include "include/utils.h"
#include "io/gpio/gpio.h"
#include "io/spi/spi.h"

/*------------
 * DATATYPES |
 *----------*/
/**
 * A CAN address - 16 bit field but can only use the first 11 bits (up to 0x7FF)
 */
typedef uint16_t CAN_addr_t;

/**
 * CAN Message Statuses
 *
 * These define the status bytes of a received CAN message. Only useful on CAN reception, and need not be set on a message for transmission.
 */
typedef enum {
    CAN_STAT_ERROR  = 0xFFFF,  ///< CAN Error
    CAN_STAT_MERROR = 0xFFFE,
    CAN_STAT_WAKE   = 0xFFFD,
    CAN_STAT_RTR    = 0xFFFC,  ///< CAN Return request packet
    CAN_STAT_OK     = 0x0001   ///< Good CAN message
} CAN_msg_status_t;

/**
 * A CAN message.
 */
typedef struct {
    CAN_msg_status_t status;    ///< Status/type of the message. Only valid on a received message
    CAN_addr_t       address;   ///< The address the message is to be transmitted on, or was received on
    group_64         data;      ///< The message's data
} CAN_message_t;

/**
 * Simple queue implementation for CAN messages
 */
typedef struct {
    CAN_message_t * push_ptr;   ///< Pointer to the location to push a message into the queue
    CAN_message_t * pop_ptr;    ///< Pointer to the location to read a message from the queue
    CAN_message_t * buf;        ///< Pointer to a CAN_message_t[] allocated either at runtime or statically in app
    uint8_t         depth;      ///< How many messages the queue can store
} CAN_queue_t;

/**
 * Possible CAN bitrates
 */
typedef enum {
   CAN_BITRATE_500K
} CAN_bitrate_t;

/**
 * Configuration options for a CAN bus.
 *
 * Typically define these statically in your application header.
 */
typedef const struct {
    SPI_module_t   spi;                 ///< The SPI bus the CAN controller communicates over
    GPIO_pin_num_t cs;                  ///< The chip select pin for the controller
    CAN_addr_t     addr;                ///< The CAN base address for this controller
    CAN_bitrate_t  rate;                ///< The bitrate for the bus this controller is connected to
    uint8_t        queue_depth;         ///< How many messages the TX & RX queues should be allocated to hold
    bool           use_interrupts;      ///< CURRENTLY UNIMPLEMENTED. If true, will register a GPIO interrupt on the interrupt pin
    GPIO_pin_num_t int_pin;             ///< CURRENTLY UNIMPLEMENTED. The controller's interrupt pin
    bool           rx_mailbox_rollover; ///< Enable a RX message to roll over into RXB1 if RXB0 is full
    uint16_t       rx0_mask;            ///< Receive address mask bits. Bits in this mask are evaluated against the filters in a received address
    CAN_addr_t     rx0_filters[2];      ///< Receive filter addresses for RXB0
    uint16_t       rx1_mask;            ///< Receive address mask bits. Bits in this mask are evaluated against the filters in a received address
    CAN_addr_t     rx1_filters[4];      ///< Receive filter addresses for RXB1
} CAN_config_t;

/**
 * A CAN bus
 */
typedef struct {
    CAN_queue_t  tx_queue;  ///< The transmit queue
    CAN_queue_t  rx_queue;  ///< The receive queue
    CAN_config_t *cfg;      ///< The current configuration for this bus
} CAN_bus_t;

/*-------------------
 * PUBLIC FUNCTIONS |
 *-----------------*/
/**
 * Initialize the MCP2515.
 *
 * @param bus A pointer to a CAN bus object to be initialized
 * @param cfg The configuration to set up on the specified CAN bus
 */
err_t CAN_init(CAN_bus_t * bus, CAN_config_t * cfg);

/**
 * Receives a CAN message from the MCP2515 in RXB0, and stores it into the bus' receive queue
 *
 *  - Run this routine when an IRQ is received / when querying the chip
 *  - Queries the controller to identify the source of the IRQ
 *      - If it was an ERROR IRQ, read & clear the Error Flag register, add an error message to RX queue
 *      - If it was an RX IRQ, read the message and address
 *      - If both, handle the error preferentially to the message
 *  - Clear the appropriate IRQ flag bits
 *  - Add either the error or the received message to the RX queue
 *
 * @param bus The CAN bus to query
 */
err_t CAN_receive(CAN_bus_t * bus);

/**
 * Increment the buffer's push pointer. Call after writing a message in the push pointer.
 *
 * @param q The queue to act on
 */
void CAN_push(CAN_queue_t * q);

/**
 * Pops the next available element out of the specified CAN buffer. Increments the buffer's pop pointer.
 *
 * @param q A CAN_queue_t from within a CAN bus to pop a message from
 *
 * @return A pointer to the next-in-line can_message object to read out of the receive queue
 */
CAN_message_t * CAN_pop(CAN_queue_t * q);

/**
 * Query if the primary CAN transmit mailbox is full.
 *
 * @param bus The CAN bus to query
 *
 * @return true if mailbox available, false if mailbox full
 */
bool CAN_isbusy(CAN_bus_t * bus);

/**
 * Put the MCP2515 into sleep mode
 *
 * @param bus The CAN bus to query
 */
err_t CAN_sleep(CAN_bus_t * bus);

/**
 * Wake the MCP2515 from sleep.
 *
 * @param bus The CAN bus to query
 */
err_t CAN_wake(CAN_bus_t * bus);

/**
 * Transmits a CAN message out of bus's TX queue to the bus.
 *
 *  - If there are packets in the Queue, pick out the next one and send it
 *  - Accepts address and data payload via can_interface structure
 *  - Uses status field to pass in DLC (length) code
 *  - Checks mailbox 1 is free, if not, returns -1 without transmitting packet
 *  - Busy waits while message is sent to CAN controller on SPI port
 *  - Only uses mailbox 1, to avoid corruption from CAN Module Errata (in Microchip DS80179G)
 *
 * @param bus The bus to transmit a packet on
 *
 * @return 1 = transmitted packet from the queue, -1 = all mailboxes busy, -2 = nothing to transmit
 */
int8_t CAN_transmit(CAN_bus_t * bus);

/*--------------------
 * CHIP DEFINITIONS  |
 *------------------*/

// MCP2515 command bytes
#define MCP_RESET       0xC0
#define MCP_READ        0x03
#define MCP_READ_RX     0x90
#define MCP_WRITE       0x02
#define MCP_WRITE_TX    0x40
#define MCP_RTS         0x80        // When used, needs to have buffer to transmit inserted into lower bits
#define MCP_STATUS      0xA0
#define MCP_FILTER      0xB0
#define MCP_MODIFY      0x05

// MCP2515 register names
#define RXF0SIDH        0x00
#define RXF0SIDL        0x01
#define RXF0EID8        0x02
#define RXF0EID0        0x03
#define RXF1SIDH        0x04
#define RXF1SIDL        0x05
#define RXF1EID8        0x06
#define RXF1EID0        0x07
#define RXF2SIDH        0x08
#define RXF2SIDL        0x09
#define RXF2EID8        0x0A
#define RXF2EID0        0x0B
#define BFPCTRL         0x0C
#define TXRTSCTRL       0x0D
#define CANSTAT         0x0E
#define CANCTRL         0x0F

#define RXF3SIDH        0x10
#define RXF3SIDL        0x11
#define RXF3EID8        0x12
#define RXF3EID0        0x13
#define RXF4SIDH        0x14
#define RXF4SIDL        0x15
#define RXF4EID8        0x16
#define RXF4EID0        0x17
#define RXF5SIDH        0x18
#define RXF5SIDL        0x19
#define RXF5EID8        0x1A
#define RXF5EID0        0x1B
#define TEC             0x1C
#define REC             0x1D

#define RXM0SIDH        0x20
#define RXM0SIDL        0x21
#define RXM0EID8        0x22
#define RXM0EID0        0x23
#define RXM1SIDH        0x24
#define RXM1SIDL        0x25
#define RXM1EID8        0x26
#define RXM1EID0        0x27
#define CNF3            0x28
#define CNF2            0x29
#define CNF1            0x2A
#define CANINTE         0x2B
#define CANINTF         0x2C
#define EFLAG           0x2D

#define TXB0CTRL        0x30
#define TXB0SIDH        0x31
#define TXB0SIDL        0x32
#define TXB0EID8        0x33
#define TXB0EID0        0x34
#define TXB0DLC         0x35
#define TXB0D0          0x36
#define TXB0D1          0x37
#define TXB0D2          0x38
#define TXB0D3          0x39
#define TXB0D4          0x3A
#define TXB0D5          0x3B
#define TXB0D6          0x3C
#define TXB0D7          0x3D

#define TXB1CTRL        0x40
#define TXB1SIDH        0x41
#define TXB1SIDL        0x42
#define TXB1EID8        0x43
#define TXB1EID0        0x44
#define TXB1DLC         0x45
#define TXB1D0          0x46
#define TXB1D1          0x47
#define TXB1D2          0x48
#define TXB1D3          0x49
#define TXB1D4          0x4A
#define TXB1D5          0x4B
#define TXB1D6          0x4C
#define TXB1D7          0x4D

#define TXB2CTRL        0x50
#define TXB2SIDH        0x51
#define TXB2SIDL        0x52
#define TXB2EID8        0x53
#define TXB2EID0        0x54
#define TXB2DLC         0x55
#define TXB2D0          0x56
#define TXB2D1          0x57
#define TXB2D2          0x58
#define TXB2D3          0x59
#define TXB2D4          0x5A
#define TXB2D5          0x5B
#define TXB2D6          0x5C
#define TXB2D7          0x5D

#define RXB0CTRL        0x60
#define RXB0SIDH        0x61
#define RXB0SIDL        0x62
#define RXB0EID8        0x63
#define RXB0EID0        0x64
#define RXB0DLC         0x65
#define RXB0D0          0x66
#define RXB0D1          0x67
#define RXB0D2          0x68
#define RXB0D3          0x69
#define RXB0D4          0x6A
#define RXB0D5          0x6B
#define RXB0D6          0x6C
#define RXB0D7          0x6D

#define RXB1CTRL        0x70
#define RXB1SIDH        0x71
#define RXB1SIDL        0x72
#define RXB1EID8        0x73
#define RXB1EID0        0x74
#define RXB1DLC         0x75
#define RXB1D0          0x76
#define RXB1D1          0x77
#define RXB1D2          0x78
#define RXB1D3          0x79
#define RXB1D4          0x7A
#define RXB1D5          0x7B
#define RXB1D6          0x7C
#define RXB1D7          0x7D

// MCP2515 RX ctrl bit definitions
#define MCP_RXB0_RTR    0x08
#define MCP_RXB0_BUKT   0x04
#define MCP_RXB1_RTR    0x08

// MCP2515 Interrupt flag register bit definitions
#define MCP_IRQ_MERR    0x80
#define MCP_IRQ_WAKE    0x40
#define MCP_IRQ_ERR     0x20
#define MCP_IRQ_TXB2    0x10
#define MCP_IRQ_TXB1    0x08
#define MCP_IRQ_TXB0    0x04
#define MCP_IRQ_RXB1    0x02
#define MCP_IRQ_RXB0    0x01

#endif /* _CAN_H */
