/**
 * @file can.c
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 13 November 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license
 * This project is released under the GNU Public License (GPLv3).
 *
 * @brief MCP2515 CAN driver
 *
 * Based heavily on Tritium's implementation from their EV driver controls.
 * Takes a more object-oriented approach.
 */

#include "io/can/can.h"
#include "io/gpio/gpio.h"
#include "io/spi/spi.h"

// Private function prototypes
static err_t    can_mod(CAN_bus_t * bus, uint8_t address, uint8_t mask, uint8_t data);
static void     can_reset(CAN_bus_t * bus);
static void     can_write(CAN_bus_t * bus, uint8_t address, uint8_t *ptr, uint8_t bytes);
static void     can_read(CAN_bus_t * bus, uint8_t address, uint8_t *ptr, uint8_t bytes);
static void     can_write_tx(CAN_bus_t * bus, uint8_t *ptr);
static err_t    can_rts(CAN_bus_t * bus, uint8_t address);
static uint8_t  can_read_status(CAN_bus_t * bus);

static uint8_t contigtx_buf[16];  // Local buffer for doing contiguous addressed writes to the controller

err_t CAN_init(CAN_bus_t * bus, CAN_config_t * cfg)
{
    // Link the config with the bus
    bus->cfg = cfg;

    // Allocate the TX and RX queues
    // NOTE: If your platform doesn't support malloc you can pretty easily change
    // the CAN_queue_t to be a statically allocated array & comment this section out.
    bus->tx_queue.buf = malloc(cfg->queue_depth * sizeof(bus->tx_queue.buf[0]));
    bus->rx_queue.buf = malloc(cfg->queue_depth * sizeof(bus->rx_queue.buf[0]));
    if(bus->rx_queue.buf == NULL || bus->tx_queue.buf == NULL) ASSERT(ENOSPACE);
    bus->tx_queue.push_ptr = bus->tx_queue.buf;
    bus->tx_queue.pop_ptr  = bus->tx_queue.buf;
    bus->tx_queue.depth    = cfg->queue_depth;
    bus->rx_queue.push_ptr = bus->rx_queue.buf;
    bus->rx_queue.pop_ptr  = bus->rx_queue.buf;
    bus->rx_queue.depth    = cfg->queue_depth;

    // Reset chip & configure it's internal clock divider
    can_reset(bus);
    for(uint16_t i = 0; i < 1000; i++); // Wait for MCP2515 to finish reset. This is fast.
    can_mod(bus, CANCTRL, 0x03, 0x00);       // CANCTRL register, modify lower two bits, clk = /1 = 20MHz

    // Set up the bitrate
    switch(cfg->rate)
    {
        case CAN_BITRATE_500K:
            contigtx_buf[0] = 0x05;   // CNF3
            contigtx_buf[1] = 0xF8;   // CNF2
            contigtx_buf[2] = 0x00;   // CNF1
            break;
    }

    // Set up interrupts
    if(cfg->use_interrupts)
    {
        // TODO: Add interrupt pin functionality setup here for interrupt pin
    }
    // NOTE: Interrupts are enabled in chip so we can read IRQ registers for event detection
    // The use_interrupts config flag is meant to decide if interrupt main MCU on pin output.
    contigtx_buf[3] = 0x23;   // CANINTE register: enable error, rx0 & rx1 interrupts on IRQ pin
    contigtx_buf[4] = 0x00;   // CANINTF register: clear all IRQ flags
    contigtx_buf[5] = 0x00;   // EFLG register: clear all user-changable error flags
    can_write(bus, CNF3, &contigtx_buf[0], 6); // Write 6 bytes starting at CNF3 register

    //Enable receive buffer rollover if it is enabled in the driver
    if(cfg->rx_mailbox_rollover)
    {
        can_mod(bus, RXB0CTRL, MCP_RXB0_BUKT, 0x04); //BUKT bit high, rollover enabled
    } else {
        can_mod(bus, RXB0CTRL, MCP_RXB0_BUKT, 0x00); //BUKT bit low, rollover disabled
    }

    // Set up receive filtering & masks
    // RXF0 - Buffer 0
    contigtx_buf[ 0] = (uint8_t)(cfg->rx0_filters[0] >> 3);  // RXF0SIDH - 8 MSb's of standard ID filter
    contigtx_buf[ 1] = (uint8_t)(cfg->rx0_filters[0] << 5);  // RXF0SIDL - 3 LSb's of standard ID filter
    contigtx_buf[ 2] = 0x00;                                 // No extended ID or data filtering
    contigtx_buf[ 3] = 0x00;
    // RXF1 - Buffer 0
    contigtx_buf[ 4] = (uint8_t)(cfg->rx0_filters[1] >> 3);  // RXF1SIDH
    contigtx_buf[ 5] = (uint8_t)(cfg->rx0_filters[1] << 5);  // RXF1SIDL
    contigtx_buf[ 6] = 0x00;
    contigtx_buf[ 7] = 0x00;
    // RXF2 - Buffer 1
    contigtx_buf[ 8] = (uint8_t)(cfg->rx1_filters[0] >> 3);  // RXF2SIDH
    contigtx_buf[ 9] = (uint8_t)(cfg->rx1_filters[0] << 5);  // RXF2SIDL
    contigtx_buf[10] = 0x00;
    contigtx_buf[11] = 0x00;
    can_write(bus, RXF0SIDH, &contigtx_buf[0], 12);

    // RXF3 - Buffer 1
    contigtx_buf[ 0] = (uint8_t)(cfg->rx1_filters[1] >> 3);  // RXF3SIDH
    contigtx_buf[ 1] = (uint8_t)(cfg->rx1_filters[1] << 5);  // RXF3SIDL
    contigtx_buf[ 2] = 0x00;
    contigtx_buf[ 3] = 0x00;
    // RXF4 - Buffer 1
    contigtx_buf[ 4] = (uint8_t)(cfg->rx1_filters[2] >> 3);  // RXF4SIDH
    contigtx_buf[ 5] = (uint8_t)(cfg->rx1_filters[2] << 5);  // RXF4SIDL
    contigtx_buf[ 6] = 0x00;
    contigtx_buf[ 7] = 0x00;
    // RXF5 - Buffer 1
    contigtx_buf[ 8] = (uint8_t)(cfg->rx1_filters[3] >> 3);  // RXF5SIDH
    contigtx_buf[ 9] = (uint8_t)(cfg->rx1_filters[3] << 5);  // RXF5SIDL
    contigtx_buf[10] = 0x00;
    contigtx_buf[11] = 0x00;
    can_write(bus,  RXF3SIDH, &contigtx_buf[0], 12 );

    // RXM0 - Buffer 0
    contigtx_buf[ 0] = (uint8_t)(cfg->rx0_mask >> 3);  // RXM0SIDH - 8 MSb's of standard ID mask
    contigtx_buf[ 1] = (uint8_t)(cfg->rx0_mask << 5);  // RXM0SIDL - 3 LSb's of standard ID mask
    contigtx_buf[ 2] = 0x00;                           // No extended ID or data masks
    contigtx_buf[ 3] = 0x00;
    // RXM1 - Buffer 1
    contigtx_buf[ 4] = (uint8_t)(cfg->rx1_mask >> 3);  // RXM1SIDH
    contigtx_buf[ 5] = (uint8_t)(cfg->rx1_mask << 5);  // RXM1SIDL
    contigtx_buf[ 6] = 0x00;
    contigtx_buf[ 7] = 0x00;
    can_write(bus, RXM0SIDH, &contigtx_buf[0], 8);

    // Switch out of config mode into normal operating mode
    can_mod(bus, CANCTRL, 0xE0, 0x00);   // CANCTRL register, modify upper 3 bits, mode = Normal

    return SUCCESS;
}

err_t CAN_receive(CAN_bus_t * bus)
{
    // Prepare status flag bitfield
    uint8_t flags;
    can_read(bus, CANINTF, &flags, 1);

    // Check for errors or wakeups
    if(flags & MCP_IRQ_ERR) // Check for errors first
    {
        can_read(bus, EFLAG, &contigtx_buf[0], 1);   // Read in error flags
        can_read(bus, TEC, &contigtx_buf[1], 2);     // Read in transmit error count and receive error count (TEC & REC)
        can_mod(bus, EFLAG, contigtx_buf[0], 0x00);  // Clear error flags we just read in

        // Generate a CAN error message & push it into the queue
        bus->rx_queue.push_ptr->status = CAN_STAT_ERROR;
        bus->rx_queue.push_ptr->address = 0x0000;
        bus->rx_queue.push_ptr->data.data_u8[0] = flags;       // CANINTF
        bus->rx_queue.push_ptr->data.data_u8[1] = contigtx_buf[0];   // EFLG
        bus->rx_queue.push_ptr->data.data_u8[2] = contigtx_buf[1];   // TEC
        bus->rx_queue.push_ptr->data.data_u8[3] = contigtx_buf[2];   // REC

        //Push the message into the CAN receive queue
        CAN_push(&bus->rx_queue);

        // Clear the IRQ flag
        can_mod(bus, CANINTF, MCP_IRQ_ERR, 0x00);

        return EHARDWARE;
    } else if(flags & MCP_IRQ_WAKE){
        // Clear the IRQ flag
        can_mod(bus, CANINTF, MCP_IRQ_WAKE, 0x00);
    }

    // No error - now we can check for received messages
    if(flags & MCP_IRQ_RXB0) // Received message in RX0 buffer
    {
        // Read in the info, address, and message data
        can_read(bus, RXB0CTRL, &contigtx_buf[0], 14);

        // Concatenate the 11-bit address
        bus->rx_queue.push_ptr->address = contigtx_buf[1] << 3 | contigtx_buf[2] >> 5;

        // Check if this was a return request - data is irrelevant if so.
        if(contigtx_buf[0] & MCP_RXB0_RTR)
        {
            bus->rx_queue.push_ptr->status = CAN_STAT_RTR;
        } else
        {
            // Read in the data from this normal packet
            bus->rx_queue.push_ptr->status = CAN_STAT_OK;
            bus->rx_queue.push_ptr->data.data_u8[0] = contigtx_buf[ 6];
            bus->rx_queue.push_ptr->data.data_u8[1] = contigtx_buf[ 7];
            bus->rx_queue.push_ptr->data.data_u8[2] = contigtx_buf[ 8];
            bus->rx_queue.push_ptr->data.data_u8[3] = contigtx_buf[ 9];
            bus->rx_queue.push_ptr->data.data_u8[4] = contigtx_buf[10];
            bus->rx_queue.push_ptr->data.data_u8[5] = contigtx_buf[11];
            bus->rx_queue.push_ptr->data.data_u8[6] = contigtx_buf[12];
            bus->rx_queue.push_ptr->data.data_u8[7] = contigtx_buf[13];
        }

        CAN_push(&bus->rx_queue);

        // Clear the IRQ flag
        can_mod(bus, CANINTF, MCP_IRQ_RXB0, 0x00);
    }
    if(flags & MCP_IRQ_RXB1) // Received message in RX0 buffer
    {
        // Read in the info, address, and message data
        can_read(bus, RXB1CTRL, &contigtx_buf[0], 14);

        // Concatenate the 11-bit address
        bus->rx_queue.push_ptr->address = contigtx_buf[1] << 3 | contigtx_buf[2] >> 5;

        // Check if this was a return request - data is irrelevant if so.
        if(contigtx_buf[0] & MCP_RXB1_RTR)
        {
            bus->rx_queue.push_ptr->status = CAN_STAT_RTR;
        } else
        {
            // Read in the data from this normal packet
            bus->rx_queue.push_ptr->status = CAN_STAT_OK;
            bus->rx_queue.push_ptr->data.data_u8[0] = contigtx_buf[ 6];
            bus->rx_queue.push_ptr->data.data_u8[1] = contigtx_buf[ 7];
            bus->rx_queue.push_ptr->data.data_u8[2] = contigtx_buf[ 8];
            bus->rx_queue.push_ptr->data.data_u8[3] = contigtx_buf[ 9];
            bus->rx_queue.push_ptr->data.data_u8[4] = contigtx_buf[10];
            bus->rx_queue.push_ptr->data.data_u8[5] = contigtx_buf[11];
            bus->rx_queue.push_ptr->data.data_u8[6] = contigtx_buf[12];
            bus->rx_queue.push_ptr->data.data_u8[7] = contigtx_buf[13];
        }

        CAN_push(&bus->rx_queue);

        // Clear the IRQ flag
        can_mod(bus, CANINTF, MCP_IRQ_RXB1, 0x00);
    }

    return SUCCESS;
}

CAN_message_t * CAN_pop(CAN_queue_t * q)
{
    CAN_message_t * ret = q->pop_ptr;

    // Increment the pointer to the next element
    q->pop_ptr++;

    // If the new location is past the end of the buffer
    if(q->pop_ptr >= (sizeof(*q->pop_ptr) * q->depth) + q->buf)
    {
        // Loop the pointer back to the start of buffer
        q->pop_ptr = q->buf;
    }

    return ret;
}

void CAN_push(CAN_queue_t * q)
{
    q->push_ptr++;

    // If the new location is past the end of the buffer
    if(q->push_ptr >= (sizeof(*q->push_ptr) * q->depth) + q->buf)
    {
        // Loop the pointer back to the start of buffer
        q->push_ptr = q->buf;
    }
}

bool CAN_isbusy(CAN_bus_t * bus)
{
    if((can_read_status(bus) & 0x04) == 0x04)
    {
        return true;
    } else return false;
}

err_t CAN_sleep(CAN_bus_t * bus)
{
    uint8_t status, count = 0;

    //Sing the MCP2515 a lullaby
    can_mod(bus, CANCTRL, 0xE0, 0x20); //CANCTRL register, modify upper 3 bits, mode = Sleep

    //Wait until it falls asleep
    while((status & 0xE0) != 0x20) {
        can_read(bus, CANSTAT, &status, 1);
        if(count > 10) return EHARDWARE;
    }

    return SUCCESS;
}

err_t CAN_wake(CAN_bus_t * bus)
{
    return can_mod(bus, CANCTRL, 0xE0, 0x00); //CANCTRL, upper 3 bits, mode=Normal
}

int8_t CAN_transmit(CAN_bus_t * bus)
{
    // Check if there are messages in the queue
    if(bus->tx_queue.push_ptr != bus->tx_queue.pop_ptr)
    {
        if(CAN_isbusy(bus))
        {
            return -1;  // No available mailbox
        } else
        {
            CAN_message_t * tx = CAN_pop(&bus->tx_queue);

            //Format the data for MCP2515
            contigtx_buf[ 0] = (uint8_t)(tx->address >> 3);
            contigtx_buf[ 1] = (uint8_t)(tx->address << 5);
            contigtx_buf[ 2] = 0x00;                        // Extended identifier bits, unused
            contigtx_buf[ 3] = 0x00;                        // Extended identifier bits, unused
            contigtx_buf[ 4] = 8;                           // Data length code, always sending 8-bit data.
            contigtx_buf[ 5] = tx->data.data_u8[0];
            contigtx_buf[ 6] = tx->data.data_u8[1];
            contigtx_buf[ 7] = tx->data.data_u8[2];
            contigtx_buf[ 8] = tx->data.data_u8[3];
            contigtx_buf[ 9] = tx->data.data_u8[4];
            contigtx_buf[10] = tx->data.data_u8[5];
            contigtx_buf[11] = tx->data.data_u8[6];
            contigtx_buf[12] = tx->data.data_u8[7];
            can_write_tx(bus, &contigtx_buf[0]);
            can_rts(bus, 0);

            return 1;
        }
    } else
    {
        return -2; // Nothing to transmit
    }
}

/*-----------------------
 * PRIVATE DEFINITIONS  |
 *---------------------*/
/**
 * Modifies selected register in MCP2515.
 *
 * @param address Register to be modified
 * @param mask Bit mask
 * @param data New bit data
 */
err_t can_mod(CAN_bus_t * bus, uint8_t address, uint8_t mask, uint8_t data)
{
    err_t ret = SUCCESS;
    ret |= GPIO_write_pin(bus->cfg->cs, 0);
    ret |= SPI_transact(bus->cfg->spi, address);
    ret |= SPI_transact(bus->cfg->spi, mask);
    ret |= SPI_transact(bus->cfg->spi, data);
    ret |= GPIO_write_pin(bus->cfg->cs, 1);
    if(ret != SUCCESS) return EHARDWARE;
    else return SUCCESS;
}

/**
 * Resets MCP2515 CAN controller via SPI port.
 *  - SPI device must be already initialized
 */
void can_reset(CAN_bus_t * bus)
{
    GPIO_write_pin(bus->cfg->cs, 0);
    SPI_transact(bus->cfg->spi, MCP_RESET);
    GPIO_write_pin(bus->cfg->cs, 1);
}

/**
 * Writes contiguous data bytes to the MCP2515.
 *
 * @param bus A pointer to a CAN bus object
 * @param address Starting address to write to in the chip
 * @param ptr Pointer to array of bytes to write
 * @param bytes Number of bytes to write
 */
void can_write(CAN_bus_t * bus, uint8_t address, uint8_t *ptr, uint8_t bytes)
{
    GPIO_write_pin(bus->cfg->cs, 0);
    SPI_transact(bus->cfg->spi, MCP_READ);
    SPI_transact(bus->cfg->spi, address);
    for(uint8_t i = 0; i < (bytes-1); i++)
    {
        SPI_transact(bus->cfg->spi, *ptr++);
    }
    GPIO_write_pin(bus->cfg->cs, 1);
}

/**
 * Reads contiguous data bytes from the MCP2515.
 *
 * @param bus A pointer to a CAN bus object
 * @param address Starting address to read from in chip
 * @param ptr Array of bytes for return data
 * @param bytes Number of bytes to read
 */
void can_read(CAN_bus_t * bus, uint8_t address, uint8_t *ptr, uint8_t bytes)
{
    GPIO_write_pin(bus->cfg->cs, 0);
    SPI_transact(bus->cfg->spi, MCP_READ);
    SPI_transact(bus->cfg->spi, address);
    for(uint8_t i = 0; i < bytes; i++)
    {
        *ptr++ = SPI_transact(bus->cfg->spi, 0); // Read in data
    }
    GPIO_write_pin(bus->cfg->cs, 1);
}

/**
 * Writes data bytes to transmit buffer 0 (TXB0) in MCP2515.
 *
 * @note This can be extended to support multiple transmit buffers by adding another parameter that
 * simply takes a uint8_t with a 0, 1, or 2 in it marking which transmit buffer to send to, and then
 * in the initial spi_tx bitwise OR MCP_WRITE_TX with the new buffer parameter.
 *
 * @param bus A pointer to a CAN bus object
 * @param ptr A 13-byte long array of data to write to the transmit buffer.
 */
void can_write_tx(CAN_bus_t * bus, uint8_t *ptr)
{
    GPIO_write_pin(bus->cfg->cs, 0);
    SPI_transact(bus->cfg->spi, MCP_WRITE_TX);
    for(uint8_t i = 0; i < 13; i++) // Always sending 13 bytes for TX register
    {
        SPI_transact(bus->cfg->spi, *ptr++);
    }
    GPIO_write_pin(bus->cfg->cs, 1);
}

/**
 * Request to send selected transmit buffer.
 *
 * @param bus A pointer to a CAN bus object
 * @param address Which buffer to transmit - 0, 1, or 2
 */
err_t can_rts(CAN_bus_t * bus, uint8_t address)
{
    if(address > 2) return EPARAMINVAL;
    GPIO_write_pin(bus->cfg->cs, 0);
    SPI_transact(bus->cfg->spi, MCP_RTS | (1 << address));
    GPIO_write_pin(bus->cfg->cs, 1);

    return SUCCESS;
}

/**
 * Reads MCP2515 status register
 *
 * @param bus A pointer to a CAN bus object
 *
 * @return The value of the status register
 */
uint8_t can_read_status(CAN_bus_t * bus)
{
    uint8_t ret;

    GPIO_write_pin(bus->cfg->cs, 0);
    SPI_transact(bus->cfg->spi, MCP_STATUS);
    ret = SPI_transact(bus->cfg->spi, 0);
    GPIO_write_pin(bus->cfg->cs, 1);

    return ret;
}
