/**
 * @file gpio.c
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 24 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @brief Kernel GPIO Interface
 *
 */

#include <stdbool.h>
#include <stdint.h>

#include "application.h"
#include "io/gpio/gpio.h"

// Map a pin number/logical name to its pin config
GPIO_config_t *prv_get_pin(GPIO_pin_num_t pin)
{
    for(uint8_t i = 0; i < GPIO_COUNT; i++)
    {
        if(app_pins[i].id == pin)
        {
            return (GPIO_config_t * const) &app_pins[i];
        }
    }
    ASSERT(ENONEXISTENT);

    // This will never actually hit, but we have to have a return to please the compiler
    return NULL;
}

err_t GPIO_init()
{
    for(uint8_t i = 0; i < GPIO_COUNT; i++)
    {
        ASSERT(GPIO_set_dir(app_pins[i].id, app_pins[i].direction));
        ASSERT(GPIO_set_function(app_pins[i].id, app_pins[i].function));
        ASSERT(GPIO_set_pull(app_pins[i].id, app_pins[i].pull));
    }

    return SUCCESS;
}

bool GPIO_read_pin(GPIO_pin_num_t pin)
{
    GPIO_config_t * mpin = prv_get_pin(pin);

    return platform_gpio_read_pin(mpin->port, mpin->index);
}

err_t GPIO_set_dir(GPIO_pin_num_t pin, GPIO_pin_dir_t dir)
{
    GPIO_config_t * mpin = prv_get_pin(pin);

    return platform_gpio_set_dir(mpin->port, mpin->index, dir);
}

err_t GPIO_set_function(GPIO_pin_num_t pin, GPIO_pin_func_t func)
{
    GPIO_config_t * mpin = prv_get_pin(pin);

    return platform_gpio_set_function(mpin->port, mpin->index, func);
}

err_t GPIO_set_pull(GPIO_pin_num_t pin, GPIO_pin_pull_t pull)
{
    GPIO_config_t * mpin = prv_get_pin(pin);

    return platform_gpio_set_pull(mpin->port, mpin->index, pull);
}

err_t GPIO_toggle_pin(GPIO_pin_num_t pin)
{
    GPIO_config_t * mpin = prv_get_pin(pin);

    return platform_gpio_toggle_pin(mpin->port, mpin->index);
}

err_t GPIO_write_pin(GPIO_pin_num_t pin, bool value)
{
    GPIO_config_t * mpin = prv_get_pin(pin);

    return platform_gpio_write_pin(mpin->port, mpin->index, value);
}
