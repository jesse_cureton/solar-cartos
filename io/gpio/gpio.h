/**
 * @file gpio.h
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 20 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @brief Kernel GPIO Interface
 *
 */
#ifndef GPIO_H_
#define GPIO_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "include/error.h"
#include "architecture/platform.h"

typedef uint8_t GPIO_pin_num_t;

typedef enum {
    GPIO_OUTPUT,
    GPIO_INPUT
} GPIO_pin_dir_t;

typedef enum {
    GPIO_FUNC_IO,
    GPIO_FUNC_PRIMARY,
    GPIO_FUNC_SECONDARY,
    GPIO_FUNC_TERTIARY
} GPIO_pin_func_t;

typedef enum {
    GPIO_PULL_OFF,
    GPIO_PULL_UP,
    GPIO_PULL_DOWN
} GPIO_pin_pull_t;

// GPIO initialization struct - these are all const to prevent unintended use after GPIO init
typedef struct {
    const GPIO_pin_num_t  id;
    const GPIO_port_t     port;
    const GPIO_pin_idx_t  index;
    const GPIO_pin_dir_t  direction;
    const GPIO_pin_func_t function;
    const GPIO_pin_pull_t pull;
} GPIO_config_t;

extern bool  platform_gpio_read_pin(GPIO_port_t port, GPIO_pin_idx_t pin);
extern err_t platform_gpio_set_dir(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_dir_t dir);
extern err_t platform_gpio_set_function(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_func_t func);
extern err_t platform_gpio_set_pull(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_pull_t pull);
extern err_t platform_gpio_toggle_pin(GPIO_port_t port, GPIO_pin_idx_t pin);
extern err_t platform_gpio_write_pin(GPIO_port_t port, GPIO_pin_idx_t pin, bool value);

//TODO: Need some way to do a word/half-word read from a whole port, and interrupt functionality.

err_t GPIO_init();
bool  GPIO_read_pin(GPIO_pin_num_t pin);
err_t GPIO_set_dir(GPIO_pin_num_t pin, GPIO_pin_dir_t dir);
err_t GPIO_set_function(GPIO_pin_num_t pin, GPIO_pin_func_t func);
err_t GPIO_set_pull(GPIO_pin_num_t pin, GPIO_pin_pull_t pull);
err_t GPIO_toggle_pin(GPIO_pin_num_t pin);
err_t GPIO_write_pin(GPIO_pin_num_t pin, bool value);

#endif /* GPIO_H_ */
