/**
 * @file gpio_readme.txt
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 17 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @page OS_MODULES_IO_GPIO GPIO - Pin Interface
 *
 * @section IO_GPIO_INTRO Introduction to GPIO
 *
 * @section IO_GPIO_API GPIO API
 * The API to use the GPIO module can be seen in {@link gpio.h}.
 *
 * @section IO_GPIO_APP_CONFIGS Application Configuration Values
 * No app configuration values are required to enable GPIO functionality, but in order to use it the app
 * must define its desired pin configuration in the form of an enum with the logical pin names/functions
 * it will be using, and an array of <code>GPIO_config_t</code>'s describing the desired configuration
 * for each logical pin. The end of the enum must be <code>GPIO_COUNT</code> to allow for pin counting.
 * An example is below:
 *
 * <pre><code>
 * enum {
 *     TEST_PIN,
 *     GPIO_COUNT
 * };
 * static const GPIO_config_t app_pins[] = {
 *   // Name,    Port,         Index,      Direction,   Function,     Pull Up/Down
 *   { TEST_PIN, GPIO_PORT_10, GPIO_PIN_5, GPIO_OUTPUT, GPIO_FUNC_IO, GPIO_PULL_OFF }
 * };
 * </code></pre>  
 *
 * @section IO_GPIO_PLATFORM_API Required Platform API
 * Each architecture implementation must implement the following functions to be GPIO compatible.
 * Example implementations can be seen in the gpio_msp432p401r code & header files. If an architecture
 * can't support one of the functions for whatever reason, it is safe to return EUNIMPLEMENTED, which
 * the application can handle however it likes.
 *
 * 1. <code>bool  platform_gpio_read_pin(GPIO_port_t port, GPIO_pin_idx_t pin)</code>
 * 2. <code>err_t platform_gpio_set_dir(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_dir_t dir)</code>
 * 3. <code>err_t platform_gpio_set_function(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_func_t func)</code>
 * 4. <code>err_t platform_gpio_set_pull(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_pull_t pull)</code>
 * 5. <code>err_t platform_gpio_toggle_pin(GPIO_port_t port, GPIO_pin_idx_t pin)</code>
 * 6. <code>err_t platform_gpio_write_pin(GPIO_port_t port, GPIO_pin_idx_t pin, bool value)</code>
 *
 * As long as the application implements these functions & their expected behavior, the backend
 * may be implemented however the author sees fit to work on that microcontroller. Finally, the platform
 * must also define to typedef'd enumerations describing its available ports and pins as follows:
 *
 * <pre><code>
 * // Enumeration of the ports exposed on the microcontroller
 * typedef enum {
 *     GPIO_PORT_1 = 0,    // Initialization to 0 isn't required, but included for clarity
 *     GPIO_PORT_2,
 *     GPIO_PORT_3,
 *     GPIO_PORT_4,
 *     GPIO_PORT_5,
 *     GPIO_PORT_6,
 *     GPIO_PORT_7,
 *     GPIO_PORT_8,
 *     GPIO_PORT_9,
 *     GPIO_PORT_10,
 *     GPIO_PORT_J
 * } GPIO_port_t;
 * 
 * // Enumeration of the pins expected to be available in each port.
 * // These must be defined as bit values to allow logical chaining for multi-pin ops
 * typedef enum {
 *     GPIO_PIN_1 = 0x01,
 *     GPIO_PIN_2 = 0x02,
 *     GPIO_PIN_3 = 0x04,
 *     GPIO_PIN_4 = 0x08,
 *     GPIO_PIN_5 = 0x10,
 *     GPIO_PIN_6 = 0x20,
 *     GPIO_PIN_7 = 0x40,
 *     GPIO_PIN_8 = 0x80
 * } GPIO_pin_idx_t;
 *
 * </code></pre>
 *
 */
