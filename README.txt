/**
 * @file README.txt
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 13 April 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 * 
 * @mainpage The Solar Car Real Time Operating System (Solar CaRTOS)
 * 
 * @section PROJECT_INTRO Introduction
 * In Spring of 2017, the Missouri S&T Solar Car Team used a mix of MSP430 and MSP432 microcontrollers
 * across more than a dozen embedded systems in our car. For years, these systems all maintained separate
 * codebases despire sharing a large amount of common code. This led to a great deal of manpower going
 * into keeping things in sync, and changes in common code sometimes taking weeks to propagate across
 * all platforms as maintainers got to them. They also had no scheduling, and portability was nearly nil.
 * \n\n
 * The Solar CaRTOS aims to remedy that. The core principals behind its design are simplicity, modularity,
 * and documentation. Since this will likely be many young developers's first foray into real-time systems
 * thorough documentation and simplicity are key to getting them off the ground quickly. The goal is to
 * provide an operating system environment and framework that merges most shared code, implements a
 * scheduler for periodic and aperiodic jobs, and abstracts out platform code to support multiple processors. 
 * 
 * @section PROJECT_GET_STARTED Getting Started with the Solar CaRTOS
 * Details on implementing this with an application: git submoduling, adding to project, OS structure, templates
 * 
 * @section PROJECT_LICENSE License and Copyright
 * Copyright (C) 2017 - Jesse Cureton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
