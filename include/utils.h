/**
 * @file utils.h
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 30 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @brief Global Utility Functions & Macros
 */

#ifndef INCLUDE_UTILS_H_
#define INCLUDE_UTILS_H_

#define ARRAY_COUNT(arr) (sizeof(arr) / sizeof(arr[0])) ///< Simple macro to get number of elements in array

/**
 * A 64-bit union of memory.
 *
 * This type can be used to create a 64 bit block of memory which can then be addressed as any 64-bit
 * combination of two float values, 8 signed/unsigned 8-bit values, 4 signed/unsigned 16-bit values,
 * 2 signed/unsigned 32-bit values, or 1 unsigned 64-bit value.
 *
 * Example:
 * <code><pre>
 * group_64 val;
 * val.data_fp[0] = (float) 42.000;
 * val.data_u16[2] = 0x4242;
 * val.data_u8[6] = 0x42;
 * val.data_u8[7] = 0x42;
 * </pre></code>
 */
typedef union _group_64 {
    float data_fp[2];
    uint8_t data_u8[8];
    int8_t data_8[8];
    uint16_t data_u16[4];
    int16_t data_16[4];
    uint32_t data_u32[2];
    int32_t data_32[2];
    uint64_t data_u64;
} group_64;

/**
 * A 32-bit union of memory.
 *
 * @see {@link group_64} for details on this implementation, as this is the same with only 32-bits of memory.
 */
typedef union _group_32 {
    float data_fp;
    unsigned char data_u8[4];
    char data_8[4];
    unsigned int data_u16[2];
    int data_16[2];
    unsigned long data_u32;
    long data_32;
} group_32;

/**
 * A 16-bit union of memory.
 *
 * @see {@link group_64} for details on this implementation, as this is the same with only 16 bits of memory.
 */
typedef union _group_16 {
    unsigned char data_u8[2];
    char data_8[2];
    unsigned int data_u16;
    int data_16;
} group_16;

#endif /* INCLUDE_UTILS_H_ */
