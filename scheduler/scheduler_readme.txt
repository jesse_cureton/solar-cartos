/**
 * @file application_interface.txt
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 8 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @page OS_MODULES_SCHEDULER SCH - Scheduler
 *
 * @section SCHEDULER_INTRO Introduction
 * The SCH module handles global task scheduling for both periodic and aperiodic jobs in the OS modules and
 * application layers. In its current iteration it implements a simple priority-driven scheduler similar to
 * a rate-monotonic scheduler. It differs in that it lacks preemptivity and adds user-driven priority support
 * as well. The lack of preemptivity means it is "run to completion". Every task you add must complete for 
 * control to return to the scheduler and the operating system.
 *
 * You can view the basic call flow of the scheduler in the @ref APPLICATION_INTERFACE_FLOWGRAPH "OS flow graph",
 * and <strong>view the scheduler API {@link scheduler.c here}</strong>
 *
 * @section SCHEDULER_EXAMPLES Examples
 * @subsection SCHEDULER_EXAMPLE_ADD_TASK Adding a Basic Periodic Task
 * <code><pre>
 * void my_func()
 * {
 *     // Run my_callback every 1000ms with no arguments at the lowest priority
 *     schAddTask(LOWEST, my_callback, NULL, 1000, SCH_TIME_NEGLIGIBLE));
 * }
 * 
 * // Note that a callback function must always have a uint32_t parameter for args, even if unused
 * void my_callback(uint32_t args)
 * {
 *	   // Just execute a no-op
 *     asm("nop");
 * }
 * </pre></code>
 * That's it. {@link SCH_TIME_NEGLIGIBLE} should be used carefully - if your job doesn't take less time than
 * the scheduler resolution (SCH_TICK - assume 1ms) to execute, then it will still get added but can cause
 * the validity test to pass when the job in actuality breaks the schedulers ability to meet deadlines.
 *
 * Your application main function should always initialize some kind of periodic task if your application
 * is to ever do anything. See the {@link APPLICATION_INTERFACE application interface} for more details.
 *
 * It can be a good idea to wrap a call to schAddTask() in an {@link ASSERT} statement. This will allow you
 * to catch an error if the scheduler couldn't add your task for one reason or another.
 *
 * @subsection SCHEDULER_EXAMPLE_ADD_ONE_OFF Adding a One-Shot Task
 * Adding a one-shot task is exactly like adding a periodic one, except the <code>period</code> parameter
 * is zero. The added one-shot will be scheduled to execute as soon as possible - accounting for its priority
 * and the priority of other jobs in the queue. It can't execute until after the current task surrenders control.
 *
 * @subsection SCHEDULER_EXAMPLE_PASSING_ARGS Passing Arguments
 * This is currently somewhat broken.. you would pass a memory address (reference) as args, and then cast that back 
 * to a pointer of the type args was expected to be, but because when this was written scope of that pointer wasn't
 * considered, if the scope owning the pointer's location goes out of scope then the argument is no longer valid
 * when the scheduled task actually executes.
 *
 * <strong>Fun side effect of that: if your argument location is global-scope or guaranteed not to go out of scope before
 * execution, then this works as intended. You can force a variable to stay in scope after a function executes by
 * declaring it static within the function.</strong>
 *
 * <code><pre>
 * void my_func()
 * {
 *     // Note that myPassedVal is declared static so it will stay in scope once my_func() exits
 *     // It could also be defined globally and modified here, but that isn't preferred.
 *	   static uint16_t myPassedVal = 0xCDEF;
 *
 *     // args parameter should be the address to the value you want to pass cast to a uint32
 *	   // (If this ever gets run on 64-bit hardware this addressing won't work, but that's far off)
 *     schAddTask(LOWEST, my_callback, (uint32_t)&myPassedVal, 1000, SCH_TIME_NEGLIGIBLE));
 * }
 * 
 * void my_callback(uint32_t args)
 * {
 *	   // args = memory address of a uint16_t in this case
 *	   uint16_t myRcvdArg = *((uint16_t *) args); // myRcvdArg = 0xCDEF
 * }
 * </pre></code>
 * 
 * In general, you pass an argument by declaring it static and casting a reference to it to a uint32. In the callback
 * you can receive this argument by casting <code>args</code> to a pointer of the type the argument should be, then
 * dereferencing that pointer.
 *
 * @subsection SCHEDULER_EXAMPLE_TASK_PROFILING Profiling Task Runtime
 * As mentioned before, when adding a task it is important to accurately submit its <code>exec</code> execution
 * time to the scheduler. For incredibly simple (1/2 line) functions, using {@link SCH_TIME_NEGLIGIBLE} is okay,
 * but for anything serious the scheduler counts on knowing a reasonable accurate execution time.
 *
 * If the execution time could vary, always plan on the worst case. Overprovisioning here isn't an issue -
 * jobs can complete sooner and the scheduler will account for it, but an overrun can prevent other jobs from
 * executing and lead to overload.
 *
 * The good news is profiling is easy - when developing your function just drive a pin high/low when control
 * first enters, and drive it to the opposite state when it exits. Then on a scope or logic analyzer, see how
 * wide the pulse is. That's your baseline execution time. Use it when adding a task.
 * 
 * @section SCHEDULER_RESOURCES Additonal Resources
 * If you're interested in more theoretical concepts behind the scheduler, check out some of the resources
 * below. Many of these were compiled during planning and development for the initial operating system dev
 * phase for a Real Time Systems course in Spring 2017. 
 *
 * - Bailey Miller, Frank Vahid, and Tony Givargis, "RIOS: A Lightweight Task Scheduler for Embedded Systems,"
 *   in <i>Proceedings of the Workshop on Embedded and Cyber-Physical Systems Education (WESE '12)</i>, Tampere,
 *   Finland, October 12, 2012.
 * - Miro Samek and Robert Ward, "Build a Super Simple Tasker," Embedded Systems Design, 2006.
 * - <a href="https://www.embedded.com/design/operating-systems/4442729/Introducing--RTOS-Revealed">RTOS Revealed</a>
 *   An Embedded Magazine guide on real-time operating system structure & functionality.
 */