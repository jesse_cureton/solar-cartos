/**
 * @file gpio_msp432p401r.h
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 20 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @brief GPIO Definitions for MSP432P401R
 */
#ifndef GPIO_MSP432P401R_H_
#define GPIO_MSP432P401R_H_

#include "architecture/platform.h"

typedef enum {
    GPIO_PORT_1 = 0,    // Initialization to 0 isn't required, but included for clarity
    GPIO_PORT_2,
    GPIO_PORT_3,
    GPIO_PORT_4,
    GPIO_PORT_5,
    GPIO_PORT_6,
    GPIO_PORT_7,
    GPIO_PORT_8,
    GPIO_PORT_9,
    GPIO_PORT_10,
    GPIO_PORT_J
} GPIO_port_t;

typedef enum {
    GPIO_PIN_0 = 0x01,  // These must be defined as such to allow logical chaining for multi-pin ops
    GPIO_PIN_1 = 0x02,
    GPIO_PIN_2 = 0x04,
    GPIO_PIN_3 = 0x08,
    GPIO_PIN_4 = 0x10,
    GPIO_PIN_5 = 0x20,
    GPIO_PIN_6 = 0x40,
    GPIO_PIN_7 = 0x80
} GPIO_pin_idx_t;

#endif
