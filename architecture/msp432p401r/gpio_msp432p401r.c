/**
 * @file gpio_msp432p401r.c
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 23 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @brief GPIO Implementations for MSP432P401R
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "architecture/platform.h"
#include "include/error.h"
#include "io/gpio/gpio.h"

/**
 * For the MSP432, two the 8-bit port register sets are interleaved like this:
 *      PA = [P1IN P2IN P1OUT P2OUT ... ]
 * The offsets are the same, but memory addresses are staggered by 1 byte.
 * We can define the base address for each port like this, then calculate the
 * offsets at compile time as defines.
 */
static const uintptr_t port_bases[] = {
    [GPIO_PORT_1]  = (uintptr_t)P1,
    [GPIO_PORT_2]  = (uintptr_t)P1 + 1,
    [GPIO_PORT_3]  = (uintptr_t)P3,
    [GPIO_PORT_4]  = (uintptr_t)P3 + 1,
    [GPIO_PORT_5]  = (uintptr_t)P5,
    [GPIO_PORT_6]  = (uintptr_t)P5 + 1,
    [GPIO_PORT_7]  = (uintptr_t)P7,
    [GPIO_PORT_8]  = (uintptr_t)P7 + 1,
    [GPIO_PORT_9]  = (uintptr_t)P9,
    [GPIO_PORT_10] = (uintptr_t)P9 + 1,
    [GPIO_PORT_J]  = (uintptr_t)PJ,
};

#define PxIN    ((uintptr_t)&P1->IN - (uintptr_t)P1)
#define PxOUT   ((uintptr_t)&P1->OUT - (uintptr_t)P1)
#define PxDIR   ((uintptr_t)&P1->DIR - (uintptr_t)P1)
#define PxREN   ((uintptr_t)&P1->REN - (uintptr_t)P1)
#define PxDS    ((uintptr_t)&P1->DS - (uintptr_t)P1)
#define PxSEL0  ((uintptr_t)&P1->SEL0 - (uintptr_t)P1)
#define PxSEL1  ((uintptr_t)&P1->SEL1 - (uintptr_t)P1)
#define PxIE    ((uintptr_t)&P1->IE - (uintptr_t)P1)
#define PxIES   ((uintptr_t)&P1->IES - (uintptr_t)P1)
#define PxIFG   ((uintptr_t)&P1->IFG - (uintptr_t)P1)
#define PxIE    ((uintptr_t)&P1->IE - (uintptr_t)P1)

bool platform_gpio_read_pin(GPIO_port_t port, GPIO_pin_idx_t pin)
{
    return ( HWREG8(port_bases[port] + PxIN) & pin );
}

err_t platform_gpio_set_dir(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_dir_t dir)
{
    switch(dir)
    {
        case GPIO_OUTPUT:
            HWREG8(port_bases[port] + PxDIR) |= pin;
            break;
        case GPIO_INPUT:
            HWREG8(port_bases[port] + PxDIR) &= ~pin;
            break;
        default:
            return EPARAMINVAL;
    }

    return SUCCESS;
}

err_t platform_gpio_set_function(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_func_t func)
{
    switch(func)
    {
        case GPIO_FUNC_IO:
            HWREG8(port_bases[port] + PxSEL0) &= ~pin;
            HWREG8(port_bases[port] + PxSEL1) &= ~pin;
            break;
        case GPIO_FUNC_PRIMARY:
            HWREG8(port_bases[port] + PxSEL0) |= pin;
            HWREG8(port_bases[port] + PxSEL1) &= ~pin;
            break;
        case GPIO_FUNC_SECONDARY:
            HWREG8(port_bases[port] + PxSEL0) &= ~pin;
            HWREG8(port_bases[port] + PxSEL1) |= pin;
            break;
        case GPIO_FUNC_TERTIARY:
            HWREG8(port_bases[port] + PxSEL0) |= pin;
            HWREG8(port_bases[port] + PxSEL1) |= pin;
            break;
        default:
            return EPARAMINVAL;
    }
    return SUCCESS;
}

err_t platform_gpio_set_pull(GPIO_port_t port, GPIO_pin_idx_t pin, GPIO_pin_pull_t pull)
{
    switch(pull)
    {
        case GPIO_PULL_OFF:
            HWREG8(port_bases[port] + PxREN) &= ~pin;
            break;
        case GPIO_PULL_UP:
            HWREG8(port_bases[port] + PxREN) |= pin;
            HWREG8(port_bases[port] + PxOUT) |= pin;
            break;
        case GPIO_PULL_DOWN:
            HWREG8(port_bases[port] + PxREN) |= pin;
            HWREG8(port_bases[port] + PxOUT) &= ~pin;
            break;
        default:
            return EPARAMINVAL;
    }

    return SUCCESS;
}

err_t platform_gpio_toggle_pin(GPIO_port_t port, GPIO_pin_idx_t pin)
{
    HWREG8(port_bases[port] + PxOUT) ^= pin;

    return SUCCESS;
}

err_t platform_gpio_write_pin(GPIO_port_t port, GPIO_pin_idx_t pin, bool value)
{
    if(value) HWREG8(port_bases[port] + PxOUT) |= pin;
    else HWREG8(port_bases[port] + PxOUT) &= ~pin;

    return SUCCESS;
}
