/**
 * @file spi_msp432p401r.c
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 30 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @brief Kernel SPI Interface
 *
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "architecture/platform.h"
#include "include/error.h"
#include "io/spi/spi.h"

static const uintptr_t spi_bases[] = {
    [SPI_A0] = EUSCI_A0_SPI_BASE,
    [SPI_A1] = EUSCI_A1_SPI_BASE,
    [SPI_A2] = EUSCI_A2_SPI_BASE,
    [SPI_A3] = EUSCI_A3_SPI_BASE,
    [SPI_B0] = EUSCI_B0_SPI_BASE,
    [SPI_B1] = EUSCI_B1_SPI_BASE,
    [SPI_B2] = EUSCI_B2_SPI_BASE,
    [SPI_B3] = EUSCI_B3_SPI_BASE,
};

// Macro to evaluate if a module is on EUSCI A or B since they have different memory maps
#define IS_SPI_A(module) (module <= SPI_A3)
#define TO_SPI_A(module) ((EUSCI_A_SPI_Type *) spi_bases[module])
#define TO_SPI_B(module) ((EUSCI_B_SPI_Type *) spi_bases[module])

err_t platform_spi_enable(SPI_module_t module)
{
    // Must be disabled to configure the module
    platform_spi_disable(module);

    if(IS_SPI_A(module))
    {
        // Set to master mode
        BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_MST_OFS) = 1;

        // Set to synchronous mode
        BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_SYNC_OFS) = 1;

        //Activate the module
        BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_SWRST_OFS) = 0;
    } else
    {
        // Set to master mode
        BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_MST_OFS) = 1;

        // Set to synchronous mode
        BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_SYNC_OFS) = 1;

        //Activate the module
        BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_SWRST_OFS) = 0;
    }

    return SUCCESS;
}

err_t platform_spi_disable(SPI_module_t module)
{
    if(IS_SPI_A(module)) BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_SWRST_OFS) = 1;
    else BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_SWRST_OFS) = 1;
    return SUCCESS;
}

err_t platform_spi_set_byte_order(SPI_module_t module, bool msb_first)
{
    if(IS_SPI_A(module)) BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_MSB_OFS) = msb_first;
    else BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_MSB_OFS) = msb_first;
    return SUCCESS;
}

err_t platform_spi_set_clkpolarity(SPI_module_t module, SPI_CPHA_t phase)
{
    if(IS_SPI_A(module))
    {
        // The module must be disabled before you set clock polarity
        if(!BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_SWRST_OFS)) return(EINVALIDOP);
        switch(phase)
        {
            case SPI_CPOL_0_ACTIVEHIGH:
                BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_CKPL_OFS) = 0;
                break;
            case SPI_CPOL_1_ACTIVELOW:
                BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_CKPL_OFS) = 1;
                break;
        }
    } else
    {
        if(!BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_SWRST_OFS)) return(EINVALIDOP);
        switch(phase)
        {
            case SPI_CPOL_0_ACTIVEHIGH:
                BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_CKPL_OFS) = 0;
                break;
            case SPI_CPOL_1_ACTIVELOW:
                BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_CKPL_OFS) = 1;
                break;
        }
    }

    return SUCCESS;
}

err_t platform_spi_set_clkphase(SPI_module_t module, SPI_CPOL_t polarity)
{
    if(IS_SPI_A(module))
    {
        // The module must be disabled before you set clock phase
        if(!BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_SWRST_OFS)) return(EINVALIDOP);
        switch(polarity)
        {
            case SPI_CPHA_0_CAPTUREFIRST:
                BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_CKPH_OFS) = 1;
                break;
            case SPI_CPHA_1_CHANGEFIRST:
                BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_CKPH_OFS) = 0;
                break;
        }
    } else
    {
        if(!BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_SWRST_OFS)) return(EINVALIDOP);
        switch(polarity)
        {
            case SPI_CPHA_0_CAPTUREFIRST:
                BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_CKPH_OFS) = 1;
                break;
            case SPI_CPHA_1_CHANGEFIRST:
                BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_CKPH_OFS) = 0;
                break;
        }
    }

    return SUCCESS;
}

extern err_t platform_spi_set_clksrc_div(SPI_module_t module, SPI_clk_src_t src, SPI_clk_div_t div)
{
    if(IS_SPI_A(module))
    {
        // Set the clock source
        //BITBAND_PERI(TO_SPI_A(module)->CTLW0, EUSCI_A_CTLW0_SSEL_OFS) = src;
        // BUG: For some reason the above bitband access doesn't seem to work here..
        TO_SPI_A(module)->CTLW0 &= ~EUSCI_A_CTLW0_SSEL_MASK;
        TO_SPI_A(module)->CTLW0 |= src << EUSCI_A_CTLW0_SSEL_OFS;

        // Set the clock div
        TO_SPI_A(module)->BRW = div;
    } else
    {
        // Set the clock source
        BITBAND_PERI(TO_SPI_B(module)->CTLW0, EUSCI_B_CTLW0_SSEL_OFS) = src;

        // Set the clock div
        TO_SPI_B(module)->BRW = div;
    }

    return SUCCESS;
}

uint8_t platform_spi_transact(SPI_module_t module, uint8_t tx)
{
    if(IS_SPI_A(module))
    {
        //TO_SPI_A(module)->IFG |= EUSCI_A_IFG_TXIFG;
        TO_SPI_A(module)->TXBUF = tx;
        while(platform_spi_is_busy(module));                    // This blocking wait is bad, but no better solution yet
        return TO_SPI_A(module)->RXBUF;
    } else
    {
        //TO_SPI_B(module)->IFG |= EUSCI_B_IFG_TXIFG;
        TO_SPI_B(module)->TXBUF = tx;
        while(platform_spi_is_busy(module));
        return TO_SPI_B(module)->RXBUF;
    }
}

bool platform_spi_is_busy(SPI_module_t module)
{
    if(IS_SPI_A(module))
    {
        return BITBAND_PERI(TO_SPI_A(module)->STATW, EUSCI_A_STATW_SPI_BUSY_OFS);
    } else
    {
        return BITBAND_PERI(TO_SPI_B(module)->STATW, EUSCI_B_STATW_SPI_BUSY_OFS);
    }
}
