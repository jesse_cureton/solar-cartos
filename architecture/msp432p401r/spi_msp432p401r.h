/**
 * @file spi_msp432p401r.h
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 30 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @brief Kernel SPI Interface
 *
 */
#ifndef SPI_MSP432P401R_H_
#define SPI_MSP432P401R_H_

#include "architecture/platform.h"

typedef enum {
    SPI_A0,
    SPI_A1,
    SPI_A2,
    SPI_A3,
    SPI_B0,
    SPI_B1,
    SPI_B2,
    SPI_B3
} SPI_module_t;

typedef enum {
    SPI_CLK_ACLK = 0x01,
    SPI_CLK_SMCLK = 0x02
} SPI_clk_src_t;

typedef uint16_t SPI_clk_div_t;

#endif /* SPI_MSP432P401R_H_ */

