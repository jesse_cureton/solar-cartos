/**
 * @file core_readme.txt
 * @author Jesse Cureton (jlcvm6@mst.edu)
 * @date 9 October 2017
 * @copyright Copyright 2017 - Jesse Cureton. All rights reserved.
 * @license This project is released under the GNU Public License (GPLv3).
 *
 * @page OS_MODULES_CORE CORE - Base System
 *
 * @section CORE_INTRO Introduction to CORE
 *
 * @section CORE_APP_CONFIGS Application Configuration Values
 * The CORE module implements the following possible configuration values exposed to the
 * {@link APPLICATION_CONFIGURATION application header}.
 *
 * 1. <code>APP_SYSCLK_FREQ</code> - The application's desired SYSCLK frequency in Hertz
 *
 * @section CORE_ERROR Error Handling 
 */